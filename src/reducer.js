import {combineReducers} from 'redux';
import {RECEIVE_ANSWERS, RECEIVE_QUESTIONS} from './actions';

const initialState = {
    questions: {
        questions: [],
    },
    answers: {

    }
};

const questions = (state = initialState.questions, action) => {
    switch (action.type) {
        case RECEIVE_QUESTIONS:
            // TODOs
        default:
            return state;
    }
};

const answers = (state = initialState.answers, action) => {
    switch (action.type) {
        case RECEIVE_ANSWERS:
            return {
                ...state,
                [action.id]: action.payload,
            };
        default:
            return state;
    }
};

export default combineReducers({
    questions,
    answers
});
