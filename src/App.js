import React, { Component } from 'react';

import Questions from './Questions';
import QuestionForm from './QuestionForm';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Questions />
          <QuestionForm />
      </div>
    );
  }
}

export default App;
