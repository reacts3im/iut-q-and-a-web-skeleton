import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {TextField, RaisedButton} from 'material-ui';
import {submitAnswer} from './actions';

class QuestionForm extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        question: PropTypes.shape({
            id:     PropTypes.number.isRequired,
            author: PropTypes.string.isRequired,
            title:  PropTypes.string.isRequired,
            text:   PropTypes.string.isRequired,
        }).isRequired,
    };

    authorRef;
    answerRef;

    onSubmit(e) {
        e.preventDefault();

        // TODO envoyer la réponse
    }

    render() {
        // TODO : Afficher le formulaire de réponse
    }
}

export default connect()(QuestionForm);
