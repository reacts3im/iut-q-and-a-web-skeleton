import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Card, CardHeader, CardText} from 'material-ui';

import AnswerForm from './AnswerForm';
import Answer from './Answer';
import {requestAnswers} from './actions';

class Question extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        question: PropTypes.shape({
            id:     PropTypes.number.isRequired,
            author: PropTypes.string.isRequired,
            title:  PropTypes.string.isRequired,
            text:   PropTypes.string.isRequired,
        }).isRequired,
        answers:  PropTypes.arrayOf(
            PropTypes.shape({
                id:     PropTypes.number.isRequired,
                author: PropTypes.string.isRequired,
                answer: PropTypes.string.isRequired,
            }).isRequired,
        ).isRequired,
    };

    componentWillMount() {
        this.props.dispatch(requestAnswers(this.props.question.id));
    }

    render() {
        return (
            <Card>
                <CardHeader
                    title={this.props.question.title}
                    subtitle={this.props.question.author}
                    actAsExpander={true}
                    showExpandableButton={true}
                />
                <CardText>
                    {this.props.question.text}
                </CardText>
                <CardText expandable={true}>

                    {/*

                      TODO : Afficher les réponses


                    */}

                    <AnswerForm question={this.props.question}/>
                </CardText>
            </Card>
        );
    }
}

export default connect(
    (state, ownProps) => ({
        answers: state.answers[ownProps.question.id] || [],
    }),
)(Question);
