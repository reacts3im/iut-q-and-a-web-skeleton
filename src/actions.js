export const RECEIVE_QUESTIONS = 'RECEIVE_QUESTIONS';
export const RECEIVE_QUESTIONS_ERROR = 'RECEIVE_QUESTIONS_ERROR';
export const requestQuestions = () => async dispatch => {
    try {
        const response = await fetch('https://iut-q-and-a.herokuapp.com/questions');
        const data     = await response.json();

        dispatch({type: RECEIVE_QUESTIONS, payload: data['hydra:member']});
    } catch (e) {
        dispatch({type: RECEIVE_QUESTIONS_ERROR, error: e});
    }
};



export const RECEIVE_ANSWERS = 'RECEIVE_ANSWERS';
export const RECEIVE_ANSWERS_ERROR = 'RECEIVE_ANSWERS_ERROR';
export const requestAnswers = id => async dispatch => {
    try {
        // TODO : Requète GET de création de la réponse sur l'URL https://iut-q-and-a.herokuapp.com/questions/${id}/answers
    } catch (e) {
        dispatch({type: RECEIVE_ANSWERS_ERROR, id: id, error: e});
    }
};



export const SUBMIT_QUESTION = 'SUBMIT_QUESTION';
export const SUBMIT_QUESTION_ERROR = 'SUBMIT_QUESTION_ERROR';
export const submitQuestion = (author, title, text) => async dispatch => {
    try {
        await fetch(
            `https://iut-q-and-a.herokuapp.com/questions`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/ld+json',
                    'Content-Type': 'application/ld+json',
                },
                body:   JSON.stringify({ author, title, text }),
            }
        );

        dispatch({type: SUBMIT_QUESTION});
        dispatch(requestQuestions());
    } catch (e) {
        dispatch({type: SUBMIT_QUESTION_ERROR, error: e});
    }
};


export const SUBMIT_ANSWER = 'SUBMIT_ANSWER';
export const SUBMIT_ANSWER_ERROR = 'SUBMIT_ANSWER_ERROR';
export const submitAnswer = (question, author, answer) => async dispatch => {
    try {
        await fetch(
            `https://iut-q-and-a.herokuapp.com/answers`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/ld+json',
                    'Content-Type': 'application/ld+json',
                },
                body:   JSON.stringify({
                    question: question['@id'],
                    author,
                    answer
                }),
            }
        );

        dispatch({type: SUBMIT_ANSWER});
        dispatch(requestAnswers(question.id));
    } catch (e) {
        dispatch({type: SUBMIT_ANSWER_ERROR, error: e});
    }
};

export const CHANGE_ANSWER_NOTE = 'CHANGE_ANSWER_NOTE';
export const CHANGE_ANSWER_NOTE_ERROR = 'CHANGE_ANSWER_NOTE_ERROR';
export const changeAnswerNote = (answer, change) => async dispatch => {
    try {
        await fetch(
            `https://iut-q-and-a.herokuapp.com${answer['@id']}`,
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/ld+json',
                    'Content-Type': 'application/ld+json',
                },
                body:   JSON.stringify({
                    ...answer,
                    note: answer.note + change,
                }),
            }
        );

        dispatch({type: CHANGE_ANSWER_NOTE});
        dispatch(requestAnswers(/(\d+)$/.exec(answer.question)[0]));
    } catch (e) {
        dispatch({type: CHANGE_ANSWER_NOTE_ERROR, error: e});
    }
};
