import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {TextField, RaisedButton} from 'material-ui';
import {submitQuestion} from './actions';

class QuestionForm extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
    };

    authorRef;
    titleRef;
    questionRef;

    onSubmit(e) {
        e.preventDefault();

        const author = this.authorRef.value;
        const title = this.titleRef.value;
        const question = this.questionRef.value

        // TODO : Lancer une action pour créer la question
    }

    render() {
        return (
            <form onSubmit={this.onSubmit.bind(this)}>
                <h2>Poser une question</h2>
                <TextField floatingLabelText="Auteur" ref={ref => this.authorRef = ref.input} />
                <TextField floatingLabelText="Titre" ref={ref => this.titleRef = ref.input} />
                <TextField floatingLabelText="Question" fullWidth={true} multiLine ref={ref => this.questionRef = ref.input.refs.input} />

                <RaisedButton label="Envoyer !" primary={true} type="submit" />
            </form>
        );
    }
}

export default connect()(QuestionForm);
