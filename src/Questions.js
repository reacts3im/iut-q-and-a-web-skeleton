import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {requestQuestions} from './actions';

import Question from './Question';

class Questions extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        questions: PropTypes.arrayOf(
            PropTypes.shape({
                id:     PropTypes.number.isRequired,
                author: PropTypes.string.isRequired,
                title:  PropTypes.string.isRequired,
                text:   PropTypes.string.isRequired,
            })
        ).isRequired
    };

    componentDidMount() {
        // TODO : Lancer une action
    }

    render() {
        // TODO
    }
}

export default connect(
    state => ({
        questions: state.questions.questions,
    })
)(Questions);
